import datetime
import logging
import os
import sys

import mysql.connector

from mysql.connector import errorcode
from fbs_runtime.application_context.PySide2 import ApplicationContext
from PySide2.QtCore import Slot, Signal, QThread, QObject
from PySide2.QtGui import QFont
from PySide2.QtSql import QSqlDatabase
from PySide2.QtWidgets import QMainWindow, QWidget, QComboBox, QLabel, QVBoxLayout, QPushButton, QDialog, QLineEdit, QTextEdit, QErrorMessage

from sireader2 import SIReaderReadout
from sql import create_tables, insert_readout




class MainWindow(QMainWindow):
    def __init__(self, widget):
        QMainWindow.__init__(self)
        self.widget = widget
        self.setWindowTitle('Orientuokis')

        self.selectPort = QPushButton('Select port')
        self.connectDB = QPushButton('Connect to DB')

        self.toolbar = self.addToolBar('Main toolbar')
        self.toolbar.setMovable(False)
        self.toolbar.addWidget(self.selectPort)
        self.toolbar.addWidget(self.connectDB)


        self.selectPort.clicked.connect(self.open_port_dialog)
        self.connectDB.clicked.connect(self.open_db_dialog)

        self.port_dialog = PortSelectDialog(self)
        self.port_dialog.accepted.connect(self.port_selected)
        self.db_connect_dialog = DBConnectDialog(self)
        self.db_connect_dialog.accepted.connect(self.db_selected)

        self.setCentralWidget(widget)

        self.port = None
        self.db = None
        self.readoutThread = None

    @Slot()
    def open_port_dialog(self):
        self.port_dialog.show()

    @Slot()
    def port_selected(self):
        self.port = self.port_dialog.port
        logging.info(f'SportIdent port selected: {self.port}')
        self.widget.portLine.setText(self.port)

        self.readoutThread = ReadoutThread(self.port, self)
        self.readoutThread.start()

    @Slot()
    def open_db_dialog(self):
        self.db_connect_dialog.show()

    @Slot()
    def db_selected(self):
        logging.info(f'Database selected: {self.db.database}')
        self.widget.dbLine.setText(self.db.database)
        create_tables(self.db)

    @Slot()
    def print_readout(self, data):
        try:
            logging.info(f'Trying to write to database: {self.db.database}')
            tcx = insert_readout(self.db, data)
        except mysql.connector.errors.ProgrammingError as err:
            tcx = None
            logging.error(f'MySQL programming error: {str(err)}')
            error = QErrorMessage()
            error.showMessage(str(err))
            error.exec_()

        punches = [f"{p[0]:3}: {datetime.datetime.isoformat(p[1])}" for p in data['punches']]
        punches_str = '\n'.join(punches)
        start_str = datetime.datetime.isoformat(data['start']) if data['start'] else ''
        finish_str = datetime.datetime.isoformat(data['finish']) if data['finish'] else ''
        text = (
            f"tcx: {tcx}\n"
            f"SI CARD: {data['card_number']}\n"
            f"START: {start_str}\n"
            f"FINISH: {finish_str}\n"
            f"PUNCHES: \n"
            f"{punches_str}"
        )
        logging.info(text)
        self.widget.readoutText.setText(text)

    def closeEvent(self, event):
        if self.readoutThread is not None:
            self.readoutThread.interrupt()
        if self.db is not None:
            self.db.close()
        logging.info(f'Closing app: {datetime.datetime.now()}')
        QMainWindow.closeEvent(self, event)


class ReadoutSignals(QObject):
    cardRead = Signal(dict)


class ReadoutThread(QThread):
    def __init__(self, port, parent):
        QThread.__init__(self, parent)
        self.port = port
        self.reader = SIReaderReadout(self.port)

        self.signals = ReadoutSignals()
        self.signals.cardRead.connect(parent.print_readout)

        self._active = True

    def interrupt(self):
        self._active = False

    def run(self):
        if self.reader is not None:
            while self._active:
                card_inserted = self.reader.poll_sicard()
                if card_inserted is True:
                    logging.info(f'Reading SI card: {self.reader.sicard}')
                    readout = self.reader.read_sicard()
                    self.reader.ack_sicard()
                    self.reader.sicard = None
                    self.signals.cardRead.emit(readout)


class CentralWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.portLine = QLineEdit()
        self.portLine.setReadOnly(True)

        self.dbLine = QLineEdit()
        self.dbLine.setReadOnly(True)

        self.readoutText = QTextEdit()
        self.readoutText.setReadOnly(True)
        self.readoutText.setCurrentFont(QFont("Courier"))

        self.layout = QVBoxLayout()
        self.layout.addWidget(QLabel("Connected to port"))
        self.layout.addWidget(self.portLine)
        self.layout.addWidget(QLabel("Connected to DB"))
        self.layout.addWidget(self.dbLine)
        self.layout.addWidget(QLabel("Latest readout"))
        self.layout.addWidget(self.readoutText)

        self.setLayout(self.layout)


class PortSelectDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.selectWidget = QComboBox()

        if sys.platform.startswith('darwin'):
            with os.scandir('/dev') as it:
                for entry in it:
                    # assume that silabs.com CP210x USB to UART bridge is used
                    if entry.name.startswith('tty.SLAB'):
                        self.selectWidget.addItem(os.path.join('/dev', entry.name))
        elif sys.platform.startswith('linux'):
            with os.scandir('/dev') as it:
                for entry in it:
                    # assume that silabs.com CP210x USB to UART bridge is used
                    if entry.name.startswith('ttyS.') or entry.name.startswith('ttyUSB.'):
                        self.selectWidget.addItem(os.path.join('/dev', entry.name))

        self.connectToPort = QPushButton("Connect to port")

        self.layout = QVBoxLayout()
        self.layout.addWidget(QLabel("Port list"))
        self.layout.addWidget(self.selectWidget)
        self.layout.addWidget(self.connectToPort)

        self.setLayout(self.layout)

        self.connectToPort.clicked.connect(self.connect_to_port)

    @Slot()
    def connect_to_port(self):
        self.port = self.selectWidget.currentText()
        self.accept()


class DBConnectDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.parent = parent

        self.layout = QVBoxLayout()
        self.dbHost = QLineEdit()
        self.dbPort = QLineEdit()
        self.dbUsername = QLineEdit()
        self.dbPassword = QLineEdit()
        self.dbPassword.setEchoMode(QLineEdit.Password)
        self.dbName = QLineEdit()
        self.connectToDB = QPushButton("Connect to DB")

        self.layout.addWidget(QLabel("DB host"))
        self.layout.addWidget(self.dbHost)
        self.layout.addWidget(QLabel("DB port"))
        self.layout.addWidget(self.dbPort)
        self.layout.addWidget(QLabel("Username"))
        self.layout.addWidget(self.dbUsername)
        self.layout.addWidget(QLabel("Password"))
        self.layout.addWidget(self.dbPassword)
        self.layout.addWidget(QLabel("DB name"))
        self.layout.addWidget(self.dbName)
        self.layout.addWidget(self.connectToDB)

        self.setLayout(self.layout)

        self.connectToDB.clicked.connect(self.connect_to_db)

    @Slot()
    def connect_to_db(self):
        try:
            kwargs = dict(
                user=self.dbUsername.text(),
                password=self.dbPassword.text(),
                host=self.dbHost.text(),
                database=self.dbName.text(),
            )
            if self.dbPort.text():
                kwargs['port'] = int(self.dbPort.text())

            logging.info(f"Trying to connect to database: {kwargs['database']}")
            self.parent.db = mysql.connector.connect(**kwargs)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                err_msg = "Something is wrong with your user name or password"
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                err_msg = "Database does not exist"
            else:
                err_msg = str(err)
            logging.error(err_msg)
            error = QErrorMessage()
            error.showMessage(err_msg)
            error.exec_()

        self.accept()


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(message)s',
                        filename='orientuokis.log',
                        level=logging.DEBUG)
    logging.info(f'App launching: {datetime.datetime.now()}')
    appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
    widget = CentralWidget()
    window = MainWindow(widget)
    window.resize(400, 600)
    window.show()
    exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
    sys.exit(exit_code)
