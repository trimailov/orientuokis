import datetime
import logging
import uuid

from mysql.connector import errors


TABLES = {}
TABLES['readouts'] = (
    "CREATE TABLE readouts ("
    # " id BINARY(16) NOT NULL,"  # UUID unique row id
    # " tcx BINARY(16) NOT NULL,"  # UUID readout transaction number
    " id VARCHAR(36) NOT NULL,"  # UUID unique row id
    " tcx VARCHAR(36) NOT NULL,"  # UUID readout transaction number
    " tcx_time DATETIME NOT NULL,"  # time of readout
    " si_card INT UNSIGNED NOT NULL,"
    " punch_no VARCHAR(10) NOT NULL,"  # VARCHAR - because there's START, FINISH, etc.
    " punch_time DATETIME,"
    " punch_seq SMALLINT UNSIGNED NOT NULL,"
    " PRIMARY KEY (id))")


def create_tables(conn):
    cur = conn.cursor()
    for table_name in TABLES.keys():
        cur.execute('SHOW TABLES LIKE "%s"', (table_name,))
        cur.fetchall()  # clean readouts
        try:
            cur.execute(TABLES[table_name])
            logging.info(f"Created table: `{table_name}`")
        except errors.ProgrammingError:
            logging.info(f"Table exists: `{table_name}`")
    cur.close()


def insert_readout(conn, data):
    cur = conn.cursor()
    insert_readout = (
        "INSERT INTO readouts "
        "(id, tcx, tcx_time, si_card, punch_no, punch_time, punch_seq) "
        "VALUES "
        "(%s, %s, %s, %s, %s, %s, %s)")

    DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

    row_id = str(uuid.uuid4())
    tcx = str(uuid.uuid4())
    tcx_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%m:%S')
    si_card = data['card_number']
    start_no = 'START'
    start = data['start']
    if start:
        start_time = start.strftime(DATE_TIME_FORMAT)
    else:
        start_time = None
    start_seq = 0

    logging.info(f"Preparing transaction: {tcx}")

    start_data = (row_id, tcx, tcx_time, si_card, start_no, start_time, start_seq)
    cur.execute(insert_readout, start_data)
    logging.info(f"Prepare data to `readouts`: {start_data}")

    punches = data['punches']
    for i, punch in enumerate(punches, start=1):
        row_id = str(uuid.uuid4())
        punch_no = str(punch[0])
        punch_time = punch[1].strftime(DATE_TIME_FORMAT)
        punch_seq = i

        punch_data = (row_id, tcx, tcx_time, si_card, punch_no, punch_time, punch_seq)
        cur.execute(insert_readout, punch_data)
        logging.info(f"Prepare data to `readouts`: {punch_data}")

    row_id = str(uuid.uuid4())
    finish_no = 'FINISH'
    finish = data['finish']
    if finish:
        finish_time = finish.strftime(DATE_TIME_FORMAT)
    else:
        finish_time = None
    finish_seq = len(punches) + 1

    finish_data = (row_id, tcx, tcx_time, si_card, finish_no, finish_time, finish_seq)
    cur.execute(insert_readout, finish_data)
    logging.info(f"Prepare data to `readouts`: {finish_data}")

    conn.commit()
    logging.info(f"Written data to 'readouts`: OK\n\n")
    cur.close()
    return tcx
