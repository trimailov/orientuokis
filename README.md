# Orientuokis

SportIdent card readout to database

# Setup

```
python3 -m venv env

env/bin/pip install -U pip setuptools
env/bin/pip install -r requirements.txt
```

# Run

```
env/bin/fbs run
```

# Deploy

```
env/bin/activate
env/bin/fbs freeze
```
